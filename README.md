# curriculo

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

### Who do I talk to? ###

* Repo owner or admin @lucaslimax
* Other community or team contact lucaslimay@gmail.com
